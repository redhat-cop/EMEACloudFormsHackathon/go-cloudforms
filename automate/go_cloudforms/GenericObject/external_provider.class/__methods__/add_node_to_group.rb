###################################
#
# CloudForms Automate Method: Add Puppet Node
#
# This method is used to create a host in puppet
#
# This method requires the following to be passed in: base_url
#                                                     username
#                                                     password
#                                                     vm_name                                                  
#                                                     any puppet_group variables
# Author: Neha Chugh
###################################
begin
 require 'net/ssh'
 require 'rest-client'
 require 'openssl'
  
   # Calls run_rake to get the variables from the puppet_group
  # These variables will be utilized by the puppet_classes within the puppet_group
  # Params:
  # +puppet_group+:: puppet_group the puppet_class is associated with
  def get_variables_from_group puppet_group
    command = start_rake_command
    command << " nodegroup:variables[\"#{puppet_group}\"]"
    variables = run_rake(command)
    $evm.log(:info, "Variables for Puppet Group #{puppet_group}: #{variables}")
    variables
  end

  def find_puppet_group public_pem, ca_pem, private_pem, puppet_master_url, puppet_group, port
    $evm.log("info", "### Testing Puppet Rest API for finding the puppet group ###")
  	timeout   = 60
  	url	  = "https://#{puppet_master_url}:#{port}/classifier-api/v1/groups"
   
    $evm.log("info", " URL is #{url}")
    result = RestClient::Request.execute({
                              			 :method  		  => :get,
                               			 :url     		  => url,
                                    	 :timeout 		  => timeout,
                                         :ssl_client_cert => OpenSSL::X509::Certificate.new(File.read(public_pem)),
                                         :ssl_client_key  => OpenSSL::PKey::RSA.new(File.read(private_pem)),
                                         :ssl_ca_file  	  => ca_pem,
                                         :verify_ssl   	  => OpenSSL::SSL::VERIFY_PEER,
                                         :headers        => { :accept => '*/*', :content_type => "application/json"}
      					})
	$evm.log('info', "Puppet group result: #{result}")
  
    puppet_grp = ""
    result = JSON.parse(result)
    
    result.each do |hs|
      if puppet_group == hs['name']
          $evm.log(:info, "Found #{hs['name']}")
          puppet_grp = hs['id']
     # else
      #  create_group public_pem, ca_pem, private_pem, puppet_master_url, puppet_group, port
      end
	end
    $evm.log(:info, "PRINTING THE VALUE OF PUPPET GROUP : #{puppet_grp}")
	puppet_grp
  end
  
  def create_group public_pem, ca_pem, private_pem, puppet_master_url, puppet_group, port
    $evm.log("info", "### Testing Puppet Rest API for creating the puppet group ###")
  	timeout   = 60
  	url	  = "https://#{puppet_master_url}:#{port}/classifier-api/v1/groups"
    
    $evm.log("info", " URL is #{url}")
    payload   = JSON.dump({
        				     "name" => puppet_group
						    })
    result = RestClient::Request.execute({
                              			 :method  		  => :post,
                               			 :url     		  => url,
                                    	 :timeout 		  => timeout,
                                         :payload         => payload,
                                         :ssl_client_cert => OpenSSL::X509::Certificate.new(File.read(public_pem)),
                                         :ssl_client_key  => OpenSSL::PKey::RSA.new(File.read(private_pem)),
                                         :ssl_ca_file  	  => ca_pem,
                                         :verify_ssl   	  => OpenSSL::SSL::VERIFY_PEER,
                                         :headers        => { :accept => '*/*', :content_type => "application/json"}
      					})
   $evm.log('info', "Puppet group #{puppet_group} created with result #{result}")
  end  

  def find_puppet_rule public_pem, ca_pem, private_pem, puppet_master_url, puppet_group,  port
    $evm.log("info", "<<< Testing Puppet Rest API for finding the puppet rule >>>")
  	timeout   = 60
	puppet_grp_id  = find_puppet_group public_pem, ca_pem, private_pem, puppet_master_url, puppet_group, port
    url	  	  = "https://#{puppet_master_url}:#{port}/classifier-api/v1/groups/#{puppet_grp_id}"
    
    $evm.log("info", " URL is #{url}")
  	
    result = RestClient::Request.execute({
                              			 :method  		  => :get,
                               			 :url     		  => url,
                                    	 :timeout 		  => timeout,
                               			 :ssl_client_cert => OpenSSL::X509::Certificate.new(File.read(public_pem)),
                                         :ssl_client_key  => OpenSSL::PKey::RSA.new(File.read(private_pem)),
                                         :ssl_ca_file  	  => ca_pem,
                                         :verify_ssl   	  => OpenSSL::SSL::VERIFY_PEER,
                                         :headers 		  => { :accept => '*/*', :content_type => "application/json"}
      									})
	$evm.log('info', "Puppet group result: #{result}")
    puppet_grp = ""
    result = JSON.parse(result)
    unless result.nil?
      puppet_grp_rule = result['rule']
	  #puppet_grp_rule = puppet_grp_rule[1]
      $evm.log(:info, "Rule is #{puppet_grp_rule}")
    else
      $evm.log(:info, " ***** This group does not have any rule present ***** ")
    end
	puppet_grp_rule
  end

  def add_node_to_group_using_rest node, public_pem, ca_pem, private_pem, puppet_master_url, puppet_group, port
  
	$evm.log("info", "!!! Testing Puppet Rest API for adding the node to the nodegroup !!!")
    
    #find the puppet group ID based on the puppet group name

    puppet_group_id = find_puppet_group public_pem, ca_pem, private_pem, puppet_master_url, puppet_group, port
         
    unless puppet_group_id.blank?
      
    	$evm.log(:info, "Puppet Group ID: #{puppet_group_id}")
   	  	timeout   = 60
		puppet_grp_rule = find_puppet_rule public_pem, ca_pem, private_pem, puppet_master_url, puppet_group, port
    	url		  = "https://#{puppet_master_url}:#{port}/classifier-api/v1/groups/#{puppet_group_id}"
    	$evm.log("info", " URL is #{url}")  
        $evm.log("info", "Node is #{node}")
       unless puppet_grp_rule.nil?
         ruleArr  = ["or",["=", ["fact","hostname"], node]]
         for i in 1..puppet_grp_rule.length-1	
           ruleArr[1+i] = puppet_grp_rule[i]
         end
       else
         ruleArr  = ["or",["=", ["fact","hostname"], node]]
       end
    	payload   = JSON.dump({
        				     "rule" => ruleArr 
						    })
	    result = RestClient::Request.execute({
                              			 :method  		  => :post,
                               			 :url     		  => url,
  										 :payload 		  => payload,
                                    	 :timeout 		  => timeout,
                               			 :ssl_client_cert => OpenSSL::X509::Certificate.new(File.read(public_pem)),
                                         :ssl_client_key  => OpenSSL::PKey::RSA.new(File.read(private_pem)),
                                         :ssl_ca_file  	  => ca_pem,
                                         :verify_ssl   	  => OpenSSL::SSL::VERIFY_PEER,
                                         :headers 		  => { :accept => '*/*', :content_type => "application/json" }
      									})
    	$evm.log('info', "Add Node result: #{result}")
		result
    else
      $evm.log(:info, "NO Group ID found based on puppet Group:#{puppet_group}") 
      send_status_update "Group ID not found"
    end
  end


# Start Here
    vmname = ""
    go = $evm.root['generic_object']
    $evm.log("info", "Generic Object is #{go}")
    service_id = go.attributes['properties']['go_service']
    $evm.log("info", " Service is #{service_id}")
    service = $evm.vmdb('service').find_by_id(service_id)
    vm = service.vms.first
    $evm.log("info","VM is #{vm}")
    node = vm.hostnames.first
   $evm.log("info", "Node is #{node}")
    puppet_group = "test_group"
    #To Fetch the values required for REST API
    puppet = $evm.vmdb('generic_object').find_by_name('puppet')
      public_pem 	    = puppet.attributes['properties']['public_key']
      ca_pem  		    = puppet.attributes['properties']['ca_certificate']
      private_pem 	    = puppet.attributes['properties']['private_key']
      puppet_master_url = puppet.attributes['properties']['url']
      port			    = puppet.attributes['properties']['port']
      result = []
      #result = find_puppet_group(public_pem, ca_pem, private_pem, puppet_master_url, puppet_group, port)
      result    = add_node_to_group_using_rest(node, public_pem, ca_pem, private_pem, puppet_master_url, puppet_group, port)
    ############
    # Exit method
    ############
    $evm.log(:info, "#{@method} Ended")
    exit MIQ_OK  
  end
