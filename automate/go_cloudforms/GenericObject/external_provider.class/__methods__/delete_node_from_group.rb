###################################
#
# CloudForms Automate Method: Add Puppet Node
#
# This method is used to create a host in puppet
#
# This method requires the following to be passed in: base_url
#                                                     username
#                                                     password
#                                                     vm_name                                                  
#                                                     any puppet_group variables
#
###################################
begin
require 'net/ssh'
require 'rest-client'
require 'openssl'
# Returns the puppet version by SSHing to the Puppet Master
  def get_puppet_version
    base_url  = $evm.object['puppet_master_url']
    username =  $evm.object['username']
    password =  $evm.object['password']
    command   = "puppet --version"
    output    = ""
      Net::SSH.start(base_url, username, :password => password) do |ssh|
        output = ssh.exec!(command)
      end
        $evm.log(:info, "Raw response from SSH: #{output}")
        output = output.split("\n") unless output.nil?
       $evm.log(:info, "Formatted response from SSH: #{output}")
       version = output.first.scan(/\d.\d*.\d*/).last
      version
  end
# Returns the puppet version by SSHing to the Puppet Master
 def get_rake_version
    base_url  = $evm.object['puppet_master_url'] 
    username =  $evm.object['username'] 
    password =  $evm.object['password'] 
    command   = "rake --version"
    output    = ""
      Net::SSH.start(base_url, username, :password => password) do |ssh|
        output = ssh.exec!(command)
      end
        $evm.log(:info, "Raw response from SSH: #{output}")
        output = output.split("\n") unless output.nil?
    $evm.log(:info, "Formatted response from SSH: #{output}")
    version = output.first.scan(/\d.\d*.\d*/).last
    version
  end

   # Calls run_rake to get the variables from the puppet_group
  # These variables will be utilized by the puppet_classes within the puppet_group
  # Params:
  # +puppet_group+:: puppet_group the puppet_class is associated with
  def get_variables_from_group puppet_group
    command = start_rake_command
    command << " nodegroup:variables[\"#{puppet_group}\"]"
    variables = run_rake(command)
    $evm.log(:info, "Variables for Puppet Group #{puppet_group}: #{variables}")
    variables
  end

def find_puppet_group public_pem, ca_pem, private_pem, puppet_master_url, puppet_group, port
  
    $evm.log("info", "### Testing Puppet Rest API for finding the puppet group ###")
  	timeout   = 60
  	url	  = "https://#{puppet_master_url}:#{port}/classifier-api/v1/groups"
    
    $evm.log("info", " URL is #{url}")
    result = RestClient::Request.execute({
                              			 :method  		  => :get,
                               			 :url     		  => url,
                                    	 :timeout 		  => timeout,
                                         :ssl_client_cert => OpenSSL::X509::Certificate.new(File.read(public_pem)),
                                         :ssl_client_key  => OpenSSL::PKey::RSA.new(File.read(private_pem)),
                                         :ssl_ca_file  	  => ca_pem,
                                         :verify_ssl   	  => OpenSSL::SSL::VERIFY_PEER,
                                         :headers        => { :accept => '*/*', :content_type => "application/json"}
      					})
	$evm.log('info', "Puppet group result: #{result}")
  
    puppet_grp = ""
    result = JSON.parse(result)
    
    result.each do |hs|
      if puppet_group == hs['name']
          $evm.log(:info, "Found #{hs['name']}")
          puppet_grp = hs['id']
      end
	end
    $evm.log(:info, "PRINTING THE VALUE OF PUPPET GROUP : #{puppet_grp}")
	puppet_grp
  
end
  
  def find_puppet_rule public_pem, ca_pem, private_pem, puppet_master_url, puppet_group,  port
  
    $evm.log("info", "<<< Testing Puppet Rest API for finding the puppet rule >>>")
  	timeout   = 60
	puppet_grp_id  = find_puppet_group public_pem, ca_pem, private_pem, puppet_master_url, puppet_group, port
    url	  	  = "https://#{puppet_master_url}:#{port}/classifier-api/v1/groups/#{puppet_grp_id}"
    
    $evm.log("info", " URL is #{url}")
  	
    result = RestClient::Request.execute({
                              			 :method  		  => :get,
                               			 :url     		  => url,
                                    	 :timeout 		  => timeout,
                               			 :ssl_client_cert => OpenSSL::X509::Certificate.new(File.read(public_pem)),
                                         :ssl_client_key  => OpenSSL::PKey::RSA.new(File.read(private_pem)),
                                         :ssl_ca_file  	  => ca_pem,
                                         :verify_ssl   	  => OpenSSL::SSL::VERIFY_PEER,
                                         :headers 		  => { :accept => '*/*', :content_type => "application/json"}
      									})
	$evm.log('info', "Puppet group result: #{result}")
  
    puppet_grp = ""
    result = JSON.parse(result)
  
    unless result.nil?
      puppet_grp_rule = result['rule']
	  #puppet_grp_rule = puppet_grp_rule[1]
      $evm.log(:info, "Rule is #{puppet_grp_rule}")
    else
      $evm.log(:info, " ***** This group does not have any rule present ***** ")
    end
  
	puppet_grp_rule
  
end

def add_node_to_group_using_rest node, public_pem, ca_pem, private_pem, puppet_master_url, puppet_group, port
  
	$evm.log("info", "!!! Testing Puppet Rest API for adding the node to the nodegroup !!!")
    
    #find the puppet group ID based on the puppet group name

    puppet_group_id = find_puppet_group public_pem, ca_pem, private_pem, puppet_master_url, puppet_group, port
         
    unless puppet_group_id.blank?
      
    	$evm.log(:info, "Puppet Group ID: #{puppet_group_id}")
   	  	timeout   = 60
		puppet_grp_rule = find_puppet_rule public_pem, ca_pem, private_pem, puppet_master_url, puppet_group, port
    	url		  = "https://#{puppet_master_url}:#{port}/classifier-api/v1/groups/#{puppet_group_id}"
    	$evm.log("info", " URL is #{url}")  
        $evm.log("info", "Node is #{node}")
       unless puppet_grp_rule.nil?
         ruleArr  = ["or",["=", ["fact","hostname"], node]]
         for i in 1..puppet_grp_rule.length-1	
           ruleArr[1+i] = puppet_grp_rule[i]
         end
       else
         ruleArr  = ["or",["=", ["fact","hostname"], node]]
       end
      
    	payload   = JSON.dump({
        				     "rule" => ruleArr 
						    })
	    result = RestClient::Request.execute({
                              			 :method  		  => :post,
                               			 :url     		  => url,
  										 :payload 		  => payload,
                                    	 :timeout 		  => timeout,
                               			 :ssl_client_cert => OpenSSL::X509::Certificate.new(File.read(public_pem)),
                                         :ssl_client_key  => OpenSSL::PKey::RSA.new(File.read(private_pem)),
                                         :ssl_ca_file  	  => ca_pem,
                                         :verify_ssl   	  => OpenSSL::SSL::VERIFY_PEER,
                                         :headers 		  => { :accept => '*/*', :content_type => "application/json" }
      									})
    	$evm.log('info', "Add Node result: #{result}")
		result
    else
      $evm.log(:info, "NO Group ID found based on puppet Group:#{puppet_group}") 
      send_status_update "Group ID not found"
    end
  end


# Start Here
vmname = ""
vm = $evm.vmdb('vm').find_by_name('nchugh-test-vm0003')
node = vm.hostnames.first
puppet_group = "test_group"
$evm.set_state_var("puppet_group", puppet_group)
version      = get_puppet_version.split('.').map{|s| s.to_i} # Take version and convert "5.5.1" to [5, 5, 1] 
rake_version = get_rake_version.split('.').map{|s| s.to_i} #Take rake version and convert "12.3.1" to [12,3,1]
$evm.set_state_var("version",version)
$evm.set_state_var("rake_version",rake_version)
#To Fetch the values required for REST API
  public_pem 	    = $evm.object['public_pem'] 
  ca_pem  		    = $evm.object['ca_pem'] 
  private_pem 	    =  $evm.object['private_pem'] 
  puppet_master_url =  $evm.object['puppet_master_url']
  port			    =  $evm.object['port']
 result = []
 #result = find_puppet_group(public_pem, ca_pem, private_pem, puppet_master_url, puppet_group, port)
 result    = add_node_to_group_using_rest(node, public_pem, ca_pem, private_pem, puppet_master_url, puppet_group, port)
 end
 ############
  # Exit method
  ############
  $evm.log(:info, "#{@method} Ended")
  exit MIQ_OK
