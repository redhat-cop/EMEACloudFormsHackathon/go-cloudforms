#
# Description: Method to list groups
#
# Author: Raul Mahiques <rmahique@redhat.com>
#
begin
  @method = 'list_groups'
  @handle = $evm
  @debug = @handle.object['debug']
  @catname = @handle.object['category']
  @handle.log("info", "#{@method} - EVM Automate method started")
 
  def logInfo(msg)
    @handle.log('info', "#{@method}: " + msg.to_s) if @debug
  end

  # Funciton to populate the dialog
  def fill_dialog_field(list, default)
    dialog_hash = {
      'sort_by'       => "none",
      'data_type'     => "string",
      'required'      => true,
      'sort_order'    => "ascending",
      'values'        => list,
      'default_value' => default
      }
    dialog_hash.each { |key, value| @handle.object[key] = value }
  end

  #########################################################
  # MAIN
  #########################################################

  @values_hash = {}
  # We see if the category exists, this is just to make sure the categories have been created in advance.
  category = @handle.vmdb(:classification).find_by_name(@catname)
  unless category.nil?
    category.entries.each do |tag|
 #     logInfo("tag.name: #{tag.name},  tag.description: #{tag.description}")
      @values_hash["#{@catname}_#{tag.name}"] = tag.description
    end
  end
  logInfo("#{@catname} list: <#{@values_hash}>")

=begin
    
  #dro =  @handle.vmdb(:generic_object_definition).find_by_name("external_provider")
  dro =  @handle.vmdb(:generic_object).all
  puppet = @handle.vmdb(:generic_object).find_by_name("puppet")
  logInfo("puppet: #{puppet.inspect}")
  rr=puppet.instance_variable_get("@object")
  logInfo("8888: #{rr.inspect}")
  logInfo("###########################")
  logInfo("8888: #{rr.inspect}")
     
 
  
  
  
  end
=end
  
  dro =  @handle.vmdb(:generic_object).all
  unless dro.nil?
#    logInfo("go definition: #{dro}    ..... #{dro.inspect}")
    dro.entries.each do |aa|
      if aa.generic_object_definition_name == "external_provider"
        logInfo("aa.name: #{aa.name}")
    ####    initialize($evm, aa.name)
       # logInfo("generic object: #{aa.aaa}")
#        logInfo("LAUNCH AAAAA: #{aa.object.methods}")
#        ee=aa.instance_variable_get("@object")
#        logInfo("LAUNCH AAAAA: #{ee}")
#        logInfo("aa.aaa call: #{aa.method_missing(:class).instance_methods}")
      end
    end
  end 


# Populate the dialog
  @defprov = ""
  logInfo("#{@catname} list: <#{@values_hash}>")
  fill_dialog_field(@values_hash,@defprov)
  dialog_field = @handle.object
  logInfo("[#{@method}] - Dialog Fields ENDED")
  exit MIQ_OK
rescue => err
  $evm.log("error", "#{@method} - [#{err}]\n #{err.backtrace.join("\n")}")
  exit MIQ_ABORT
end
