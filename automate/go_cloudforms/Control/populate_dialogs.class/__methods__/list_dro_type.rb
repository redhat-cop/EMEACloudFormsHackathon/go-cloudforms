#
# Description: Method to list Generic Object instances types
#
# Author: Raul Mahiques <rmahique@redhat.com>
#
begin
  @method = 'list_dro_type'
  @handle = $evm
  @debug = @handle.object['debug']
  @goc = @handle.object['generic_object_definition'] ||= "external_provider"
  @handle.log("info", "#{@method} - EVM Automate method started")
 
  def logInfo(msg)
    @handle.log('info', "#{@method}: " + msg.to_s) if @debug
  end

  # Funciton to populate the dialog
  def fill_dialog_field(list, default)
    dialog_hash = {
      'sort_by'       => "none",
      'data_type'     => "string",
      'required'      => true,
      'sort_order'    => "ascending",
      'values'        => list,
      'default_value' => default
      }
    dialog_hash.each { |key, value| @handle.object[key] = value }
  end

  #########################################################
  # MAIN
  #########################################################
  
  
  @values_hash = {}
  # We see if the category exists, this is just to make sure the categories have been created in advance.
  dro_inst = @handle.vmdb(:generic_object).all
  unless dro_inst.nil?
    logInfo("dro_inst: #{dro_inst}")
    dro_inst.entries.each do |ins|
      if ins.generic_object_definition_name == @goc
   #     logInfo("tag.name: #{tag.name},  tag.description: #{tag.description}")
        unless ins.type.nil?
          @values_hash["#{ins.type}"] = ins.type
        end
      end
    end
  end
  logInfo("# list: <#{@values_hash}>")
 
  # Populate the dialog
  @defprov = ""
  fill_dialog_field(@values_hash,@defprov)
  dialog_field = @handle.object
  logInfo("[#{@method}] - Dialog Fields ENDED")
  exit MIQ_OK
rescue => err
  $evm.log("error", "#{@method} - [#{err}]\n #{err.backtrace.join("\n")}")
  exit MIQ_ABORT
end
