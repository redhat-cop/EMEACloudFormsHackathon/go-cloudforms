# Using Generic Objects to ease the integration with external providers.


## Short Summary

What we would like to achieve is to register the Cloudforms' vms in the
external providers that are not yet part of the ManageIQ. Generic objects will
be utilised as an external provider information storage.

Along the way of achieving this, the following Cloudforms areas are covered:
 - service dialoags
 - automate inline methods
 - automate playbook methods
 - calling a ansible playbook
 - EVM Instantiation


## Value of Solution

Using generic objects makes it possible to write down the functions that act
upon the collection of generic objects, invoking the provider-specific
functionality that is used to register the VM.

Two main benefits of using generic objects in this case are:

  * Being able to extend the capabilities of CloudForms: supporting a new
    external provider without being forced to write an a full-blown CloudForms
    provider from scratch.
  * Storing provider-specific information in a single place: all
    functionality, specific to a provider, is stored in a single place
    (generic object instance).


## Description

 - [Done] Creation of a Service Dialog to add a vm to a load balancer (AWS or F5)
 - [Done] Creation of Automate Domain with the needed Classes, Instances, Methods
 - [Done] Creation of Playbook to configure load balancer
 - [Open Issue] Getting the VM data for non-provisioning calls to the playbooks
 
There are two ways to add a vm to a load balancer. Either during provisioning 
   or by manually calling the service dialog on any given time. 

## Lessons Learned and Issues Encountered

Currently, calling generic object methods directly from the other automate
methods is not possible. There is a workaround for this issue is to write the
generic object methods in a particular way (**TODO: document the code
layout**) and then embed the generic object method in the other automate
methods.

Another thing that is currently problematic is storing any kind of
credentials in the generic object, since the generic object definition does
not have a password type, there is another workaround for this.

## Links

Current Repos:
 - https://gitlab.com/redhat-cop/EMEACloudFormsHackathon/go-cloudforms
 - https://github.com/cNeha/Ansible_with_ELB
